#--------------------------------------------------------------------------------------------------
# Standard modularization template
# --------------------------------
#
# The script will start execution from <QTDIR>.
#
# Available variables:
#     $qtdir    = <QTDIR> or where you started the modularize script from
#     $basepath = path for the modularize script, and basepath for the modularization repo
#     $OStype   = <n> where <n> is one of 0 = Windows, 1 = Unix, 2 = Mac
#
# To execute a command:
#     run("git mv foo bar")        # runs command, reports error possition and dies
#     runNotDie("git mv foo bar")  # runs command, reports error possition, returns error code
#                                    and continues. Use ($? >> 8) to get the real exit code.
#     ensureDir("qtbase")        # Ensures that qtbase exists and is a directory. Will create
#                                    it if it does not exist.
#--------------------------------------------------------------------------------------------------

my $debugScript = 0;

my %dep_to_sync_profile_map = (
    "QtCore" => "qtbase/sync.profile",
    "QtGui" => "qtbase/sync.profile",
    "QtXml" => "qtbase/sync.profile",
    "QtSql" => "qtbase/sync.profile",
    "QtTest" => "qtbase/sync.profile",
    "QtNetwork" => "qtbase/sync.profile",
    "QtDBus" => "qtbase/sync.profile",
    "QtOpenGL" => "qtbase/sync.profile",
    "QtOpenVG" => "qtbase/sync.profile",
    "QtUiTools" => "qtbase/sync.profile",
    "QtXmlPatterns" => "qtxmlpatterns/sync.profile",
    "phonon" => "qtphonon/sync.profile",
    "QtSvg" => "qtsvg/sync.profile",
    "QtDesigner" => "qttools/sync.profile",
    "QtHelp" => "qttools/sync.profile",
    "assistant" => "qttools/sync.profile",
    "designer" => "qttools/sync.profile",
    "linguist" => "qttools/sync.profile",
    "qtdemo" => "qtdoc/sync.profile",
    "qdoc3" => "qtdoc/sync.profile",
    "QtScript" => "qtscript/sync.profile",
    "QtScriptTools" => "qtscript/sync.profile",
    "qtwebkit-examples-and-demos" => "qtwebkit-examples-and-demos/sync.profile",
    "Qt3Support" => "qt3support/sync.profile",
    "QtWebKit" => "qtwebkit/Source/sync.profile",
    "QtWebKit-tests" => "qtwebkit/Source/sync.profile",
    "QtDeclarative" => "qtdeclarative/sync.profile",
    "QtMultimedia" => "qtmultimedia/sync.profile",
    "ActiveQt" => "qtactiveqt/sync.profile",
);
# Reverse map as well, with key = string, value = (@modules).
my %sync_profile_to_dep_map;
foreach my $key (keys %dep_to_sync_profile_map) {
    if (!defined($sync_profile_to_dep_map{$dep_to_sync_profile_map{$key}})) {
        $sync_profile_to_dep_map{$dep_to_sync_profile_map{$key}} = [];
    }
    push(@{$sync_profile_to_dep_map{$dep_to_sync_profile_map{$key}}}, $key);
}

my %deps = (
    "QtCore" => [],
    "QtGui" => ["QtCore"],
    "QtXml" => ["QtCore"],
    "QtSql" => ["QtCore"],
    "QtTest" => ["QtCore"],
    "QtNetwork" => ["QtCore"],
    "QtDBus" => ["QtCore", "QtXml"],
    "QtOpenGL" => ["QtCore", "QtGui"],
    "QtOpenVG" => ["QtCore", "QtGui"],
    "QtUiTools" => ["QtCore", "QtGui"],
    "QtXmlPatterns" => ["QtCore", "QtNetwork"],
    "phonon" => ["QtCore", "QtXml", "QtDBus", "QtGui"],
    "QtSvg" => ["QtCore", "QtGui"],
    "QtDesigner" => ["QtCore", "QtGui", "QtScript", "QtXml"],
    "QtHelp" => ["QtCore", "QtGui", "QtSql", "QtXml"],
    "assistant" => ["QtWebKit", "QtHelp", "QtNetwork", "QtGui", "QtSql", "QtXml", "QtCore"],
    "designer" => ["QtNetwork", "QtGui",  "QtXml", "QtDesigner", "QtScript", "QtCore"],
    "linguist" => ["QtGui",  "QtDesigner", "QtUiTools", "QtXml", "QtCore"],
    "qtdemo" => ["QtHelp", "QtXml", "QtOpenGL", "QtDeclarative", "QtScript", "QtSvg", "QtGui", "QtXmlPatterns", "QtNetwork", "QtSql", "QtCore"],
    "qdoc3" => ["QtXml", "QtCore"],
    "QtScript" => ["QtCore"],
    "QtScriptTools" => ["QtCore", "QtGui", "QtScript"],
    "qtwebkit-examples-and-demos" => ["QtWebKit"],
    "Qt3Support" => ["QtNetwork", "QtGui", "QtSql", "QtXml", "QtCore"],
    "QtWebKit" => ["QtDBus", "QtXml", "QtGui", "QtXmlPatterns", "QtNetwork", "QtCore", "phonon", "QtDeclarative"],
    "QtWebKit-tests" => ["QtWebKit", "QtDeclarative"],
    "QtDeclarative" => ["QtScript", "QtSvg", "QtGui", "QtXmlPatterns", "QtNetwork", "QtSql", "QtCore"],
    "QtMultimedia" => ["QtCore", "QtGui"],
    "ActiveQt" => ["QtCore", "QtGui"],
);

my %done_deps;

chdir("qt");

#sub run {
#    print("@_\n");
#}

sub resolveAndCommitDependencies {
    my $dep = $_[0];
    print("Entering $dep\n") if ($debugScript);
    my @dependees;
    my $syncProfile = $dep_to_sync_profile_map{$dep};
    my @sameSyncProfileModules = @{$sync_profile_to_dep_map{$syncProfile}};
    foreach my $sameSyncProfileModule (@sameSyncProfileModules) {
        push(@dependees, @{$deps{$sameSyncProfileModule}});
    }
    if ($debugScript) {
        print("vars: syncProfile=$syncProfile, "
              . "sameSyncProfileModules=@sameSyncProfileModules, "
              . "dependees=@dependees\n");
    }
    foreach my $dependee (@dependees) {
        next if ($syncProfile eq $dep_to_sync_profile_map{$dependee});
        if (!$done_deps{$dependee}) {
            resolveAndCommitDependencies($dependee);
        }
    }

    return if ($done_deps{$dep});

    print("Finishing $dep\n") if ($debugScript);

    my $syncProfileFd;
    open($syncProfileFd, ">> $syncProfile") or die("Could not open $syncProfile");
    print($syncProfileFd "# Modules and programs, and their dependencies.\n");
    print($syncProfileFd "# Each of the module version specifiers can take one of the following values:\n");
    print($syncProfileFd "#   - A specific Git revision.\n");
    print($syncProfileFd "#   - \"LATEST_REVISION\", to always test against the latest revision.\n");
    print($syncProfileFd "#   - \"LATEST_RELEASE\", to always test against the latest public release.\n");
    print($syncProfileFd "#   - \"THIS_REPOSITORY\", to indicate that the module is in this repository.\n");
    print($syncProfileFd "\%dependencies = (\n");
    foreach my $module (@sameSyncProfileModules) {
        print($syncProfileFd "    \"$module\" => {\n");
        foreach my $dependee (@{$deps{$module}}) {
            my $rev = $done_deps{$dependee} ? $done_deps{$dependee} : "THIS_REPOSITORY";
            $rev =~ s/[\r\n]//g;
            print($syncProfileFd "        \"$dependee\" => \"$rev\",\n");
        }
        print($syncProfileFd "    },\n");
    }
    print($syncProfileFd ");\n");
    close($syncProfileFd);

    $syncProfile =~ m,^([^/]+)/(.+)$, or die("Shouldn't happen! Check the module maps!");
    my $repo = $1;
    my $syncProInsideRepo = $2;
    chdir($repo);
    run("git add $syncProInsideRepo");
    run("git commit -q --author=\"axis <qt-info\@nokia.com>\" "
        . "-m \"Added dependency information to the sync.profile.\"");
    my $committedSha;
    if ($repo eq "qtwebkit") {
        # In the case of webkit, use latest revision, since we cannot publish SHAs without going through
        # the WebKit contribution process.
        $committedSha = "LATEST_REVISION";
        # Also, go back one commit, because we don't want the newly generated commit to be referenced in
        # the mother repo.
        run("git checkout -q qt-modularization-base~1");
    } else {
        run("git push origin master") if ($run_push);
        $committedSha = `git rev-parse HEAD`;
    }
    chdir("..");

    foreach my $sameSyncProfileModule (@sameSyncProfileModules) {
        $done_deps{$sameSyncProfileModule} = $committedSha;
    }
}

foreach my $dep (keys %deps) {
    resolveAndCommitDependencies($dep);
}

run("git commit -aq --author=\"axis <qt-info\@nokia.com>\" "
    . "-m \"Committed dependency information to all modules.\"");
run("git push origin master") if ($run_push);

run("cd qtwebkit && git checkout -q qt-modularization-base");

return 1;
