#--------------------------------------------------------------------------------------------------
# Helper functions for standalone applications, such as examples and demos.
# -------------------------------------------------------------------------

my %module2DirectoryMapping = (
    declarative => "qtdeclarative",
    opengl => "qtbase",
    openvg => "qtbase",
    dbus => "qtbase",
    sql => "qtbase",
    multimedia => "qtmultimedia",
    phonon => "qtphonon",
    network => "qtbase",
    svg => "qtsvg",
    webkit => "qtwebkit",
    xml => "qtbase",
    xmlpatterns => "qtxmlpatterns",
    qt3support => "qt3support",
    script => "qtscript",
    scripttools => "qtscript",
    core => "qtbase",
    gui => "qtbase",
    testlib => "qtbase"
    );

# Takes a module name and returns its intended directory in modularized Qt.
sub mapModule2Directory {
    return $module2DirectoryMapping{$_[0]};
}

# Takes a directory, scans it for profiles, and tries to figure out where it should
# go, based on the usage of "QT += xxx".
# Returns undef if it couldn't find anything, or dies if there is an error.
sub findDirectoryForApplication {
    my $dir = $_[0];

    die ("$dir is not a directory") if (!-d $dir);

    my @profiles = findFiles($dir, ".*\\.pro", 0);
    return undef if (scalar(@profiles) != 1);
    my $profile = "$dir/$profiles[0]";

    my $profileFd;
    my @modules;
    my $lineSplit = 0;
    open($profileFd, "< $profile") or die ("Could not open $profile");
    while (<$profileFd>) {
        chomp;
        if (/QT\s*\+=\s*([^\\#]*)/ || ($lineSplit && m/([^\\#]*)/)) {
            push(@modules, split(/ +/, $1));
            /\\$/ and $lineSplit = 1 or $lineSplit = 0;
            next;
        } elsif (/TEMPLATE\s*=\s*subdirs/) {
            my @subdirs = findFiles($dir, ".*", 1);
            my $targetForAll;
            foreach my $subdir (@subdirs) {
                next if (! -d $subdir);
                my $subTarget;
                $subTarget = findDirectoryForApplication($subdir);
                next if (!defined($subTarget));
                if (!defined($subTarget) || (defined($targetForAll) && $targetForAll !~ $subTarget)) {
                    return undef;
                }
                $targetForAll = $subTarget;
            }
            return $targetForAll;
        }
        $lineSplit = 0;
    }
    close($profileFd);

    my $moduleDir = "qtbase";
    foreach $module (@modules) {
        next if (!$module);
        $module =~ s/[\n\r]//g;
        my $maybeDir = mapModule2Directory($module);
        if (!defined($maybeDir)) {
            die("No mapping for module in $dir: $module\n");
        }
        if ($maybeDir !~ "qtbase") {
            if ($maybeDir !~ $moduleDir && $moduleDir !~ "qtbase") {
                # This means we found more than one non-qtbase module.
                die ("Incompatible modules in $dir: " . join(" ", @modules) . ". Do this one manually.");
            } else {
                $moduleDir = $maybeDir;
            }
        }
    }
    return $moduleDir;
}
